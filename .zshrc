function henkspngpostproc () {
    local resize_by
    local img
    resize_by="$1"
    shift
    echo "Creating Directories …"
    mkdir --parents resized/pngnqed/optipnged optimized
    for img in $*
    do
        echo "Resizing image …"
        convert -resize $resize_by% "$img" "resized/$img"
        echo "Running pngnq …"
        pngnq -s1 -f -d "resized/pngnqed" -e .png "resized/$img"
        echo "Running optipng …"
        optipng -force -out "resized/pngnqed/optipnged/$img" "resized/pngnqed/$img"
        echo "Moving final image to directory 'optimized' …"
        cp --verbose --interactive --target-directory=optimized/ "resized/pngnqed/optipnged/$img"
        echo "Removing temporary images …"
        #rm --verbose --interactive \
        rm --verbose \
          "resized/$img" \
          "resized/pngnqed/$img" \
          "resized/pngnqed/optipnged/$img"
    done
    rmdir --parents resized/pngnqed/optipnged
}

